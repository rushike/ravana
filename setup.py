import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirement.txt", "r") as rq:
    rq_txt = rq.read()
    rq_list = rq_txt.split()

setuptools.setup(
    name="ravana",
    version="0.0.2",
    packages=['ravana'],
    author="rushike",
    author_email="rushike.ab1@gmail.com",
    description="**ravana**, library for music",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/rushike/ravana",
    install_requires=rq_list,
    # packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)