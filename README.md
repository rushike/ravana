# ravana
python library to edit and analyse music in midi and wav format.

# Classes

## `ravana.midi.math.Number`
It is `int` like object, capable of doing all integer object, also has class method to find the gcd of two `Number` objects
```python
>>> from ravana.midi.math import Number
>>> num = Number(12)
>>> print(num)
```
<details>
<summary>Output</summary>
<pre>
Number : 12
</pre>
</details>

### `ravana.midi.math.Number@gcd`
Class method of class `Number` able to compute **GCD** of two `Number` objects
```python
>>> from ravana.midi.math import Number
>>> num = Number(12)
>>> num2 = Number(64)
>>> gcd = Number.gcd(num, num2)
```

<details>
<summary>Output</summary>
<pre>
Number : 4
</pre>
</details>

## `ravana.midi.data_structure.NoteSequence`
This is same class as from `rmidi` package, full path `rmidi.dataset.NoteSequence`
```python
>>> from ravana.midi.data_structure import NoteSequence
>>> ns = NoteSquence(<midi-file-path> | dict)
>>> print(type(ns))
```
<details>
<summary>Output</summary>
<pre>
class 'rmidi.dataset.note_sequence.NoteSequence
</pre>
</details>

## `ravana.midi.data_structure.sFlat`
It is the **Note** only datastructure, this datastructure excludes time assosiated with note, and note are loaded in sequence in array like objects.
```python
>>> from ravana.midi.data_structure import NoteSequence, sFlat
>>> ns = NoteSequence(<midi-file-path>)
>>> flat = sFlat(ns)
>>> print(flat)
```
<details>
<summary>Output</summary>
<pre>
sFlat : [Number : 79 Number : 78 Number : 79 Number : 86 Number : 79 Number : 78
 Number : 79 Number : 79 Number : 79 Number : 78 Number : 79 Number : 86
 Number : 79 Number : 78 Number : 79 Number : 79 Number : 79 Number : 78
 Number : 79 Number : 86 Number : 79 Number : 78 Number : 78 Number : 74
 Number : 76 Number : 78 Number : 79 Number : 78 Number : 79 Number : 86
 Number : 79 Number : 78 Number : 79 Number : 79 Number : 79 Number : 78
 Number : 79 Number : 86 Number : 79 Number : 78 Number : 79 Number : 79
 Number : 79 Number : 78 Number : 79 Number : 86 Number : 79 Number : 78
 Number : 78 Number : 83 Number : 81 Number : 83 Number : 76 Number : 78
 Number : 79 Number : 83 Number : 81 Number : 83 Number : 76 Number : 78
 Number : 79 Number : 86 Number : 84 Number : 84 Number : 88 Number : 86
 Number : 81 Number : 81 Number : 83 Number : 81 Number : 83 Number : 76
 Number : 78 Number : 79 Number : 83 Number : 81 Number : 83 Number : 76
 Number : 78 Number : 79 Number : 86 Number : 84 Number : 84 Number : 88
 Number : 86 Number : 81 Number : 81 Number : 74 Number : 78 Number : 76
 Number : 78 Number : 79 Number : 76 Number : 86 Number : 84 Number : 84
 Number : 83 Number : 83 Number : 81 Number : 81 Number : 74 Number : 76
 Number : 78 Number : 74 Number : 84 Number : 83 Number : 83 Number : 81
 Number : 81 Number : 79 Number : 79 Number : 78 Number : 76 Number : 78
 Number : 79 Number : 76 Number : 84 Number : 83 Number : 83 Number : 81
 Number : 81 Number : 79 Number : 79 Number : 78 Number : 74 Number : 76
 Number : 78 Number : 74 Number : 84 Number : 83 Number : 83 Number : 81
 Number : 81 Number : 79 Number : 79 Number : 78 Number : 76 Number : 78
 Number : 79 Number : 76 Number : 86 Number : 86 Number : 84 Number : 83
 Number : 84 Number : 74 Number : 76 Number : 78 Number : 74 Number : 84
 Number : 84 Number : 83 Number : 81 Number : 83 Number : 76 Number : 78
 Number : 79 Number : 76 Number : 86 Number : 86 Number : 84 Number : 83
 Number : 84 Number : 74 Number : 76 Number : 78 Number : 74 Number : 84
 Number : 84 Number : 83 Number : 81 Number : 83 Number : 81 Number : 79
 Number : 78 Number : 83 Number : 81 Number : 84 Number : 83 Number : 83
 Number : 88 Number : 86 Number : 84 Number : 83 Number : 83 Number : 83
 Number : 81 Number : 84 Number : 83 Number : 79 Number : 83 Number : 74
 Number : 78 Number : 83 Number : 81 Number : 84 Number : 83 Number : 83
 Number : 88 Number : 86 Number : 84 Number : 83 Number : 83 Number : 83
 Number : 81 Number : 84 Number : 83 Number : 79 Number : 83 Number : 81
 Number : 76 Number : 78 Number : 79 Number : 76 Number : 86 Number : 84
 Number : 84 Number : 83 Number : 83 Number : 81 Number : 81 Number : 74
 Number : 76 Number : 78 Number : 74 Number : 84 Number : 83 Number : 83
 Number : 81 Number : 81 Number : 79 Number : 79 Number : 78 Number : 76
 Number : 78 Number : 79 Number : 76 Number : 84 Number : 83 Number : 83
 Number : 81 Number : 81 Number : 79 Number : 79 Number : 78 Number : 74
 Number : 76 Number : 78 Number : 74 Number : 84 Number : 83 Number : 83
 Number : 81 Number : 81 Number : 79 Number : 79 Number : 78 Number : 76
 Number : 78 Number : 79 Number : 76 Number : 86 Number : 86 Number : 84
 Number : 83 Number : 84 Number : 74 Number : 76 Number : 78 Number : 74
 Number : 84 Number : 84 Number : 83 Number : 81 Number : 83 Number : 76
 Number : 78 Number : 79 Number : 76 Number : 86 Number : 86 Number : 84
 Number : 83 Number : 84 Number : 74 Number : 76 Number : 78 Number : 74
 Number : 84 Number : 84 Number : 83 Number : 81 Number : 83 Number : 81
 Number : 79 Number : 78 Number : 76 Number : 71 Number : 76 Number : 81
 Number : 79 Number : 78 Number : 76 Number : 74 Number : 74 Number : 76
 Number : 71 Number : 76 Number : 81 Number : 69 Number : 66 Number : 64
 Number : 62 Number : 66 Number : 76 Number : 71 Number : 76 Number : 81
 Number : 79 Number : 78 Number : 76 Number : 74 Number : 74 Number : 76
 Number : 71 Number : 76 Number : 81 Number : 69 Number : 66 Number : 64
 Number : 62 Number : 66 Number : 78 Number : 83 Number : 81 Number : 84
 Number : 83 Number : 83 Number : 83 Number : 88 Number : 86 Number : 84
 Number : 83 Number : 83 Number : 83 Number : 83 Number : 81 Number : 84
 Number : 83 Number : 79 Number : 83 Number : 81 Number : 74 Number : 78
 Number : 78 Number : 83 Number : 81 Number : 84 Number : 83 Number : 83
 Number : 83 Number : 88 Number : 86 Number : 84 Number : 83 Number : 83
 Number : 83 Number : 83 Number : 81 Number : 84 Number : 83 Number : 79
 Number : 83 Number : 81 Number : 76 Number : 78 Number : 79 Number : 76
 Number : 86 Number : 84 Number : 84 Number : 83 Number : 83 Number : 81
 Number : 81 Number : 74 Number : 76 Number : 78 Number : 74 Number : 84
 Number : 83 Number : 83 Number : 81 Number : 81 Number : 79 Number : 79
 Number : 78 Number : 76 Number : 78 Number : 79 Number : 76 Number : 84
 Number : 83 Number : 83 Number : 81 Number : 81 Number : 79 Number : 79
 Number : 78 Number : 74 Number : 76 Number : 78 Number : 74 Number : 84
 Number : 83 Number : 83 Number : 81 Number : 81 Number : 79 Number : 79
 Number : 78 Number : 76 Number : 78 Number : 79 Number : 76 Number : 86
 Number : 86 Number : 84 Number : 83 Number : 84 Number : 74 Number : 76
 Number : 78 Number : 74 Number : 84 Number : 84 Number : 83 Number : 81
 Number : 83 Number : 76 Number : 78 Number : 79 Number : 76 Number : 86
 Number : 86 Number : 84 Number : 83 Number : 84 Number : 74 Number : 76
 Number : 78 Number : 74 Number : 84 Number : 84 Number : 83 Number : 81
 Number : 83 Number : 81 Number : 79 Number : 78 Number : 79 Number : 78
 Number : 79 Number : 86 Number : 79 Number : 78 Number : 79 Number : 79
 Number : 79 Number : 78 Number : 79 Number : 86 Number : 79 Number : 78
 Number : 79 Number : 79 Number : 79 Number : 78 Number : 79 Number : 86
 Number : 79 Number : 78 Number : 78 Number : 74 Number : 76 Number : 78
 Number : 79 Number : 78 Number : 79 Number : 86 Number : 79 Number : 78
 Number : 79 Number : 79 Number : 79 Number : 78 Number : 79 Number : 86
 Number : 79 Number : 78 Number : 79 Number : 79 Number : 79 Number : 78
 Number : 79 Number : 86 Number : 79 Number : 78 Number : 78 Number : 76
 Number : 78 Number : 79 Number : 81 Number : 83 Number : 84 Number : 86
 Number : 88 Number : 59 Number : 64 Number : 71 Number : 76]>
</pre>
</details>

### `data_structure.sFlat@differentiate`
