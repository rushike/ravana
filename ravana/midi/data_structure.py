import music21, numpy, math
from collections import Iterable

from rmidi.dataset import NoteSequence

from ravana.midi.constant import MUSIC21, notes
from ravana.midi.math import Number, Differentiator, NORM
import jsonpickle

class music21NoteSequence:
    def __init__(self, filename, ignore = True, oftype = 'list', value = True):
        midiopener = music21.midi.MidiFile()
        midiopener.open(filename)
        midiopener.read()
        self.ns = midiopener
        self.ns = self.make(ignore)
        self.list = tuple(map(lambda track: tuple(map( lambda event: event[1] if value else event, track[1].items())), self.ns.items()))
        midiopener.close()

        self.ignore = ignore
        self.oftype = oftype
        self.value = value

    def make(self, ignore):
        result = {}
        k = 0
        for i, track in enumerate(self.ns.tracks):
            result[i] = {}
            for j, event in enumerate(track.events):
                if event.type.lower() in notes:
                    result[i][k] = self.event_dict(event.type, event.channel, event.time, event.pitch, event.velocity, event.data)
                    k += 1
        return result 

    def event_dict(self, type, channel, time, pitch, velocity, data):
        return {
                'type' : type.lower(),
                'channel' : channel,
                'code' : self.code(type, channel),
                'time' : time,  
                'pitch' : pitch, 
                'velocity' : velocity,
                'data' : data
                }

    def sequence(self, track, start, end, value = True):
        return tuple(map( lambda x : x[1], self[track][start : end]))        

    def length(self, track):
        return len(self[track])

    def track(self, track):
        return self.ns.tracks[track].events

    def code(self, typestr, channel):
        typ = MUSIC21[typestr]
        if 'META' in typ:
            return "FF{}{}".format(channel, typ['META'])
        elif 'CHANNEL' in typ:
            return "{}{}".format(typ['CHANNEL'], channel)
        elif 'SYS' in typ:
            return "{}{}".format(typ['CHANNEL'], channel)
        elif 'DELTATIME' == typestr: return
        else: raise AttributeError("Not a registered MIDI event with TYPE : {} , and with CHANNEL : {} ".format(typestr, channel))

    def channel(self, track, channel_no):
        return list(filter(lambda event: event.channel == channel_no, self.ns.tracks[track].events))

    def __len__(self):
        return len(self.ns)

    def __getitem__(self, index):
        try:
            if not isinstance(index, Iterable) : return self.list[index]           
            track, event = index
            return self.list[track][event]
        except KeyError as K:
            raise KeyError(f"Index Out of Bound, getting event, track of the midi  {K}")
    def __repr__(self):
        return str(self.ns) + " : " 


class sFlat:
    def __init__(self, ns : NoteSequence, track = 0, deriv = 0):
        if len(ns) < track : raise AttributeError(f"NoteSequence don't has atleast {track} tracks")
        if isinstance(ns, NoteSequence):            
            self.sflat = numpy.array([Number(event['pitch']) for event in ns.notes[track]])
        if isinstance(ns, Iterable):
            self.sflat = numpy.array([Number(a) for a in ns])
        self.track = track
        self.deriv = deriv

    def randomindex(self, type = "reciprocal")-> numpy.ndarray:  
        """Determines the randomness present in the sequence by using gcd-pi approximation
        type :
            'bell' --> [0, 1]
            'reciprocal --> [0, infinity]
            'normal' --> [-infinity, infinity]
        Keyword Arguments:
            type {str} -- [description] (default: {"reciprocal"})
        
        Returns:
            numpy.ndarray -- [description]
        """
        count = [0]
        fa = self.sflat
        count.extend([(1 if Number.gcd(fa[i - 1], fa[i]).note == 1 else 0) for i in range(fa.shape[0])])
        random_index_pi = (math.sqrt(6 / (sum(count) / len(count))))
        return NORM[type](random_index_pi, mu = math.pi)

    def json(self):
        return {'sflat' : [e.json() for e in self.sflat], "deriv" : self.deriv}

    def __differentiate__(self, order = 1):
        return sFlat(Differentiator(self, order).__differentiate__(itemcls = Number), track = self.track, deriv = order)

    def __integrate__(self):
        pass

    def __len__(self):
        return len(self.sflat)

    def __getitem__(self, index):
        return self.sflat[index]
    
    def __repr__(self):
        return f"<sFlat : {str(self.sflat)}>"
    
    def __str__(self):
        return str(self.sflat)


class RangeList(tuple):
    def __init__(self, rangelist):
        if not isinstance(rangelist, Iterable) and isinstance(rangelist, int): rangelist = [0 ,rangelist]
        self.list = tuple(range(rangelist[i - 1], rangelist[i]) for i in range(1, len(rangelist)))
    
    def json(self):
        return {'rlist' : self.list}
    
    def __new__(self, *args):
        return tuple.__new__(RangeList, *args)

    def __str__(self):
        return f"{self.list}"
    
    def __repr__(self):
        return f"<RangeList : {self.list}>"