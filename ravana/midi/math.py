import math, numpy, copy
from collections.abc import Iterable

def nCr(N, k):
        if N < k : raise AttributeError(f"N > k ? given --> N:{N} < k:{k}")
        return math.factorial(N) // (math.factorial(k) * math.factorial(N- k))

class ComplexNumber():
    def __init__(self, z  = (0, 0), note  = None, NROOT = 128):
        self.NROOT = NROOT
        self.z = z
        if note:
            # print(b)
            a = math.e ** (complex(0, 1) * (math.tau * note) / NROOT)
            # print(type(a))
            self.z = a.real, a.imag

    def arc(self):
        return (math.atan(self.z[1] / self.z[0]) + math.tau) % math.tau

    def __getitem__(self, index):
        return self.z[index]

    def note(self):
        return int(numpy.round(numpy.log(complex(*self.z)).imag * self.NROOT / math.tau, 6)) % self.NROOT
    def pitch(self):
        return int(numpy.round(numpy.log(complex(*self.z)).imag  * self.NROOT / math.tau, 6)) % self.NROOT

    def opposite(self):
        return Number(note = self.note() + self.NROOT/2)

    def __sub__(self, other):
        pass
    
    def __lt__(self, other):
        return self.pitch() < other.pitch() 
    def __gt__(self, other):
        return self.pitch() > other.pitch()
    def __eq__(self, other):
        return self.pitch() == other.pitch()  
    def __le__(self, other):
        return self.pitch() <= other.pitch()  
    def __ge__(self, other):
        return self.pitch() >= other.pitch()  
    def __repr__(self):
        return str(self.z)

class NoteCNumber(ComplexNumber):
    def __init__(self, note):
        super().__init__( note = note)

class Number:
    NOTEON = 0
    NOTEOFF = 1

    def __init__(self, note, time = 4):
        if type(note) == str:
            pass
        if isinstance(note, self.__class__):
            self.note = int(note.note) if note.note != math.inf else note.note
        else:
            self.note = note
        self.pitch = self.note
        self.time = time
    

    def json(self, whole = False):
        if whole:
            return self.__dict__
        return self.note

    def nonecondition(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF

    def __add__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note + other.note)

    def __mul__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note * other.note)    

    def __sub__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note - other.note)

    def __truediv__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note / other.note)

    def __floordiv__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note // other.note)

    def __mod__(self, other):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        if other in [NOTEON, None, NOTEOFF]: return NOTEOFF
        return Number(self.note % other.note)
    
    def __abs__(self):
        if self in [NOTEON, None, NOTEOFF]: return NOTEON
        return Number(self.note)
    
    def __lt__(self, other):
        if not self or not other : return False
        return self.pitch < other.pitch 

    def __gt__(self, other):
        if not self or not other : return False 
        return self.pitch > other.pitch
    
    def __eq__(self, other):
        if not self or not other : return False        
        if isinstance(self, Number) and isinstance(other, Number): return False
        return self.pitch == other.pitch  
    
    def __le__(self, other):
        if not self or not other : return False
        return self.pitch <= other.pitch  
    
    def __ge__(self, other):
        if not self or not other : return False
        return self.pitch >= other.pitch  
    
    def __str__(self):
        return f"{self.pitch}"

    def __repr__(self):
        return f"Number : {self.note}"
    
    @classmethod
    def gcd(cls, self, other):
        if isinstance(self, cls) and isinstance(other, cls):
            if self.note in [NOTEON.note, NOTEOFF.note]: return NOTEON
            if other.note in [NOTEON.note, NOTEOFF.note]: return NOTEOFF
            return Number(math.gcd(self.note, other.note))
        if isinstance(self, int) and isinstance(other, int):
            return math.gcd(self, other)

    @classmethod
    def sqrt(cls, self, other, MAX = 128):
        if isinstance(self, cls) and isinstance(other, cls):
            if self.note in [NOTEON.note, NOTEOFF.note]: return NOTEON
            if other.note in [NOTEON.note, NOTEOFF.note]: return NOTEOFF
            return Number(math.gcd(self.note, other.note))
        if isinstance(self, int) and isinstance(other, int):
            return math.gcd(self, other)

    

class Differentiator:
    """
        1
        -----
        1   0
        -    1
        -----
        1  -1    0
        -    1   -1
        -----
        1  -2    1    0
        -    1   -2    1
        ------
        1   -3    3    -1    0
        -     1   -3     3   -1
        ------
        1   -4    6    -4    1    0
        -     1   -4     6   -4    1              
        ------
        1   -5     10    -10    5     -1

        General f^{n}_{r} = (-1) ^ r  ^{n}C_{r}    ...  0 <= r <= n

        where f^{n} is derivative polynomial
    """
    def __init__(self, seq : Iterable, order = 1):
        self.seq = seq
        self.order = order
    
    def kernel(self, order, cls):
        if not cls : cls = int
        return numpy.array([cls((-1) ** k * nCr(N = order, k = k)) for k in range(order + 1)])
    
    def __differentiate__(self, seq = None, order = None, itemcls = None):
        if not seq and not order:
            seq = self.seq
            order = self.order
        kernel = self.kernel(order = order, cls = itemcls)
        conv = numpy.convolve(seq, kernel)
        conv[:order] = NOTEON
        return conv

class Integrator:
    def __init__(self, y : Iterable, z : Iterable, order = 1):
        self.y = numpy.array(y)
        self.z = numpy.array(z)
        self.order = order
    
    def kernel(self, order, cls):
        if not cls : cls = int
        return numpy.array([cls(nCr(N = order, k = k)) for k in range(order + 1)])
    
    def __integrate__(self, y, z = [], order = None, itemcls = None):
        if not y and len(z) == 0 and not order:
            y = self.y
            y.append(self.z)
            order = self.order
        else :
            y.append(z)
        kernel = self.kernel(order = order, cls = itemcls)
        conv = numpy.convolve(y, kernel)
        return conv


"""
Functions
"""
def bell(x, mu = 0, std = 1):
    return (math.pow(math.e, -(((x - mu) / std) ** 2) / 2)) / (std * math.sqrt(2 * math.pi))

def reciprocal(x, mu = 0, std = 1):
    return 1 / (x - mu)

def identity(x, mu = 0, std = 1):
    return x

NORM = {'bell' : bell, 'reciprocal' : reciprocal, 'normal' : identity, 'identity' : identity}
"""
Constant
"""

NOTEON = Number(math.inf)
NOTEOFF = Number(-math.inf)



"""
Tests
"""
# --------------------------------------------------------
# Operator Testing
# --------------------------------------------------------
# k = 0 
# for i in range(1, 128):
#     a = Number(note=i)
#     for j in range(1, 128):
#         b = Number(note=j)
#         if (a > b) <= (i > j):
#             k += 1
#         else : print((i, j), (a.pitch(), b.pitch()))

# print(k, 128 * 128 - 255, False == False, True == True)


# --------------------------------------------------------
# GCD
# --------------------------------------------------------
# a = Number(12787868688673225252266666128)
# b = Number(11128)
# print(gcd(a, b))
