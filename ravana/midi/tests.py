# import music21.midi
# from music21.midi import channelVoiceMessages, channelModeMessages
# import os
# from ravana.midi.data_structure import music21NoteSequence, sFlat
# from ravana.midi.analysis import FrequentSequence
import numpy, matplotlib.pyplot as plt
numpy.set_printoptions(edgeitems = 10)
F = "midis/Kuch_Toh_Hai_1.mid"

# midiopener = music21.midi.MidiFile()
# midiopener.open(F)
# midiopener.read()

# print(type(midiopener))

# track = midiopener.tracks[1]
# print(track)
# print(track.events[-2], track.events[-11],  track.events[-2].type )

# ns = NoteSequence(F)
# nst = sFlat(ns)

# print((nst))
# li = numpy.zeros(300)
# for i in range(300):
#     a = (nst.__differentiate__(i))
#     li[i] = a.randomindex()
#     print(li[i])


# plt.plot(numpy.arange(300), li)
# plt.show()
# print(f"list : mean : {numpy.mean(li)},  std : {numpy.std(li)}")

"""
================================================================================
Test for rmidi.Notesequence
================================================================================
"""
def test_rmidi_ns(filepath = "midis/Kuch_Toh_Hai_1.mid"):
    from ravana.midi.data_structure import music21NoteSequence, NoteSequence
    ns = music21NoteSequence(filepath)
    for i in range(len(ns)):
        for j in range(len(ns[i])):
            ns[i : i + 1, j : j + 1]
            ns[i][j]
# test_rmidi_ns()

"""
================================================================================
Test rmidi.Notesequence
================================================================================
"""
def test_rmidi_ns_to_sFlat(filepath="midis/Kuch_Toh_Hai_1.mid"):
    from ravana.midi.data_structure import NoteSequence, sFlat
    ns = NoteSequence(filepath)
    nst = sFlat(ns)
    li = numpy.zeros(100)
    for i in range(100):
        a = (nst.__differentiate__(i))
        li[i] = a.randomindex()
    plt.plot(numpy.arange(100), li)
    plt.show()
    print(f"list : mean : {numpy.mean(li)},  std : {numpy.std(li)}")
test_rmidi_ns_to_sFlat()