from ravana.midi.data_structure import NoteSequence
from ravana.midi import utils


import json, pickle, numpy, matplotlib.pyplot as plt
import threading, sys

class FrequentSequence:
    def __init__(self):
        self.data = None

    def analyse(self, ns : NoteSequence, filename):
        result = self.make(ns)
        with open(filename, 'w') as fp:
            json.dump(result, fp)
    
    def club_hist(self, dict_, xrange = [(1, 2), (2, 3), (3, 5), (5, 10), (10, sys.maxsize)], bins = {'key' : [(1, 4), (4, 12), (12, 16), (16, 24), (24, sys.maxsize)], 'freq' : [(0, 1), (1, 2), (3, 5), (5, 10), (10, sys.maxsize)]}, filename = "data_plot.png", plot = True):
        print("bins keys : ", bins['key'])
        for i in range(len(bins['key'])) :
            print("i bin key : ", bins['key'][i])
            keys = list(filter(lambda value: bins['key'][i][0] <= len(value[0].split('>')) < bins['key'][i][1] and bins['freq'][i][1] > value[1] >= bins['freq'][i][0], dict_.items()))
            print( "type : ", type(keys), "  len : ", len(keys))
            result = {}
            for x in xrange:
                result[x] = 0
                range_ = range(*x)
                print("---------> ", result, " | ", range_)
                for k, v in keys:
                    if v in range_: 
                        result[x] += 1
                        continue
            print("result : ", result, list(map(lambda x: str(x) , result.keys())), list(result.values()))        
            if plot:
                plt.bar(list(map(lambda x: str(x) , result.keys())), list(result.values()))
                plt.savefig("{}_{}".format(i, filename))
            

    def keystr(self, list):
        return ">".join(map(lambda note: utils.midi_to_note(note['pitch']), list))

    def make(self, ns, start = 0, end = None):
        li = list()
        result = {}
        if not end : end = ns.length(track = 0)
        t = 0
        for i in range(start, end):
            for j in range(i + 1, ns.length(track = 0)):
                index = self.keystr(ns.sequence(0, i, j))
                if index in result: 
                    result[index] += 1
                    t += 1
                    continue
                result[index] = 1
                t += 1   
            print("{}:>{}>>>> {}:<>:{}".format(i, j, start, end))       
        print("len iter {},  dict len {} ".format(t, len(result.keys())))
        return result
        

    def quants(self, plot = True):
        self.read('data.json')
        values = list(self.data.values())
        print("type : ", type(values), "  len : ", len(values))
        # threading.Thread(target=self.hist, kwargs={"dict_" : self.data, "keylen" : (2, 16), "freqcount" : (2, 16), "filename" : "data_main.png"}).start()
        threading.Thread(target=self.club_hist, kwargs={"dict_" : self.data, "filename" : "aggregated.png"}).start()
        

    def hist(self, dict_, keylen, freqcount, filename, plot = True):
        print("dict : ",list(dict_.items())[5])
        keys = list(filter(lambda value: keylen[0] <= len(value[0].split('>')) < keylen[1] and freqcount[1] > value[1] >= freqcount[0], dict_.items()))
        print( "type : ", type(keys), "  len : ", len(keys))
        result = {}
        for k, v in keys:
            if v in result: 
                result[v] += 1
                continue
            result[v] = 1
        if plot:
            plt.bar(list(result.keys()), list(result.values()))
            plt.savefig(filename)
        
        
    def numpy_hist(self, values, filename, plot = True):
        hist, _ = numpy.histogram(values, bins = 1)
        if plot:
            plt.bar(values, hist)
            plt.savefig(filename)

    def read(self, filename):
        with open(filename, 'r') as fp:
            data = json.load(fp)
        self.data = data
        return data